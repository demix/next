# Benchmark Result

### Server

-|fast-react|preact|preact-replace|react|react-static|react15
---|---|---|---|---|---|---
1000 times|2.75|5.75|5.75|7.75|4.25|10.00
200000 times|394.25|566.00|566.00|673.00|400.50|1045.75
### Client

-|fast-react|preact|preact-replace|react|react-static|react15
---|---|---|---|---|---|---
1000 times|parse: 15.00,load: 81.50|parse: 12.25,load: 45.00|parse: 13.50,load: 54.00|parse: 11.25,load: 75.75|parse: 17.50,load: 83.50|parse: 18.50,load: 91.25
200000 times|parse: 1654.25,load: 11706.00|parse: 2079.75,load: 12429.75|parse: 2188.25,load: 12146.00|parse: 858.25,load: 12610.25|parse: 1735.50,load: 12200.00|parse: 2726.25,load: 14476.00